﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WUA_SQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PageList : Page
    {

        public PageList()
        {
            this.InitializeComponent();
            FillItemsList();

            this.buttonMake.IsEnabled = listbox1.SelectedItems.Any();


        }



        public void FillItemsList()
        {
            List<Item> items = new ItemsRepository().GetItems();

            listbox1.Items.Clear();

            foreach (Item item in items)
            {
                ListBoxItem listItem = new ListBoxItem();
                listItem.Content = string.Format("{0} ({1}) ", item.Name, item.Description);
                listbox1.Items.Add(listItem);
            }
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((App.Current as App).isUserAuthorized)
                Frame.Navigate(typeof(PageNew));
            else
                Frame.Navigate(typeof(PageLogin));

        }

        private async void buttonMake_Click(object sender, RoutedEventArgs e)
        {
            if (!listbox1.SelectedItems.Any()) return;
            //string selectedItem = ((ListBoxItem)listbox1.SelectedItem).Content.ToString();

            Frame.Navigate(typeof (PageWorking));

        }

        private void listbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.buttonMake.IsEnabled = listbox1.SelectedItems.Any();
        }

        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            FillItemsList();

        }

    }
}
