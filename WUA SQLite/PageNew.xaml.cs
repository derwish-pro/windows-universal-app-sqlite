﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WUA_SQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PageNew : Page
    {
        public PageNew()
        {
            this.InitializeComponent();
            this.buttonAdd.IsEnabled = this.textBoxItem.Text != "" && this.textBoxDescription.Text != "";

        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {

            Item item = new Item
            {
                Name = this.textBoxItem.Text,
                Description = this.textBoxDescription.Text
            };

            new ItemsRepository().AddItem(item);


            //todo временный костыль
            if (item.Name == "drop")
                new ItemsRepository().DropDatabase();

            Frame.GoBack();
        }

        private void textBoxItem_TextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            this.buttonAdd.IsEnabled = this.textBoxItem.Text != "" && this.textBoxDescription.Text != "";
        }




    }
}
