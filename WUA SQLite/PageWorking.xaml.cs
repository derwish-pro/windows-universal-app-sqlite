﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WUA_SQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PageWorking : Page
    {
        private DispatcherTimer timer;
        private int progress;

        public PageWorking()
        {
            this.InitializeComponent();

            this.progressBar.Value = 0;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(50);
            timer.Tick += Timer_Tick;
            timer.Start();


        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            Frame.GoBack();
        }


        private void Timer_Tick(object sender, object e)
        {
            progress++;
            this.progressBar.Value = progress;
            if (progress >= 100)
            {
                timer.Stop();
                Frame.GoBack();
            }
        }
    }
}
