﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WUA_SQLite
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PageLogin : Page
    {

        private string username = "111";
        private string password = "222";

        public PageLogin()
        {
            this.InitializeComponent();
            this.textMessage.Visibility = Visibility.Collapsed;
            this.buttonLogin.IsEnabled = this.textBoxName.Text != "" ;

        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            string enteredUsername = this.textBoxName.Text;
            string enteredPassword = this.textBoxPassword.Password;

            if (enteredPassword != password || enteredUsername != username)
            {
                this.textMessage.Visibility = Visibility.Visible;

            }
            else
            {
                this.textMessage.Visibility = Visibility.Collapsed;
                (App.Current as App).isUserAuthorized = true;

                Frame.Navigate(typeof (PageNew));
                Frame.BackStack.Remove(Frame.BackStack.LastOrDefault());
            }
        }

        private void textBoxName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.textMessage.Visibility = Visibility.Collapsed;
            this.buttonLogin.IsEnabled = this.textBoxName.Text != "" ;

        }

        private void textBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.textMessage.Visibility = Visibility.Collapsed;
            this.buttonLogin.IsEnabled = this.textBoxName.Text != "" ;

        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

    }
}