﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;
using SQLite.Net;
using SQLite.Net.Platform.WinRT;

namespace WUA_SQLite
{
    class ItemsRepository
    {
        SQLiteConnection conn;

        public ItemsRepository()
        {
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, "db.sqlite");

            conn = new SQLiteConnection(new SQLitePlatformWinRT(), path);


            conn.CreateTable<Item>();
        }

        public List<Item> GetItems()
        {
            var items = conn.Table<Item>().ToList();

            return items;
        }

        public void AddItem(Item item)
        {
            var s = conn.Insert(item);
        }

        public void DropDatabase()
        {
            conn.DropTable<Item>();

        }

    }
}
